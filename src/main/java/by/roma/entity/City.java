package by.roma.entity;

import javax.persistence.*;

@Entity
@Table(name = "spr_city")
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name_en")
    private String nameEn;
    @Column(name = "name_ru")
    private String nameRu;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;

    public City() {
    }

    public City(String nameRu) {
        this.nameRu = nameRu;
    }

    public City(String nameEn, String nameRu, Country country) {
        this.nameEn = nameEn;
        this.nameRu = nameRu;
        this.country = country;
    }

    public Long getId() {
        return id;
    }


    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public Country getCountryId() {
        return country;
    }

    public void setCountryId(Country countryId) {
        this.country = countryId;
    }
}
