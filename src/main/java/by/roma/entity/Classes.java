package by.roma.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "spr_classes")
public class Classes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "description_ru")
    private String descriptionRu;
    @Column(name = "description_en")
    private String descriptionEn;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "classType")
    private List<Reservation> reservation;


    public Classes() {
    }

    public Classes(String name, String descriptionRu, String descriptionEn) {
        this.name = name;
        this.descriptionRu = descriptionRu;
        this.descriptionEn = descriptionEn;
    }


    public Long getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptionRu() {
        return descriptionRu;
    }

    public void setDescriptionRu(String descriptionRu) {
        this.descriptionRu = descriptionRu;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }
}
