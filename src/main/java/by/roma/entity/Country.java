package by.roma.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "spr_country")
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name_en")
    private String nameEn;
    @Column (name = "name_ru")
    private String nameRu;
    @Column(name = "short_name")
    private String shortName;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "country")
    private List<City> cities;

    public Country() {
    }

    public Country(String nameEn, String nameRu, String shortName) {
        this.nameEn = nameEn;
        this.nameRu = nameRu;
        this.shortName = shortName;

    }

    public Long getId() {
        return id;
    }


    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameRu() {
        return nameRu;
    }

    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

}
