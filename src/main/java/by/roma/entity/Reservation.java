package by.roma.entity;

import javax.persistence.*;

@Entity
@Table(name = "reservation")
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "flight_id")
    private Flight flight;

    @Column(name = "row")
    private String row;
    @Column(name = "seat")
    private Integer seat;

    @ManyToOne(cascade = CascadeType.ALL)
    private Passenger passenger;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "class_id")
    private Classes classType;

    public Reservation() {
    }

    public Reservation(Flight flight, String row, Integer seat, Passenger passenger, Classes classType) {
        this.flight = flight;
        this.row = row;
        this.seat = seat;
        this.passenger = passenger;
        this.classType = classType;
    }

    public Long getId() {
        return id;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public Integer getSeat() {
        return seat;
    }

    public void setSeat(Integer seat) {
        this.seat = seat;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public Classes getClassType() {
        return classType;
    }

    public void setClassType(Classes classType) {
        this.classType = classType;
    }
}
