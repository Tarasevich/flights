package by.roma.entity;



import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "spr_aircraft")
public class Aircraft {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "number")
    private String number;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "aircraft")
    private List<Flight> flights;

    @Column(name = "place_count")
    private Integer placeCount;

    @OneToOne()
    private Company company;

    @Column(name = "place_bis")
    private Integer placeBussinessCount;

    @Column(name = "place_eco_comf")
    private Integer placeEconomComfortCount;

    @Column(name = "place_eco")
    private Integer placeEconomCount;


    public Aircraft() {
    }


    public Aircraft(String name, String number, Integer placeCount, Integer placeBussinessCount, Integer placeEconomComfortCount, Integer placeEconomCount) {
        this.name = name;
        this.number = number;
        this.placeCount = placeCount;
        this.placeBussinessCount = placeBussinessCount;
        this.placeEconomComfortCount = placeEconomComfortCount;
        this.placeEconomCount = placeEconomCount;
    }

    public Aircraft(String name, String number, Integer placeCount, Company company, Integer placeBisunessCount, Integer placeEconomComfortCount, Integer placeEconomCount) {
        this.name = name;
        this.number = number;
        this.placeCount = placeCount;
        this.company = company;
        this.placeBussinessCount = placeBisunessCount;
        this.placeEconomComfortCount = placeEconomComfortCount;
        this.placeEconomCount = placeEconomCount;
        this.company = company;
    }


    public Long getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPlaceCount() {
        return placeCount;
    }

    public void setPlaceCount(Integer placeCount) {
        this.placeCount = placeCount;
    }

    public Company getCompany() {
        return company;
    }


    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Integer getPlaceBussinessCount() {
        return placeBussinessCount;
    }

    public void setPlaceBussinessCount(Integer placeBussinessCount) {
        this.placeBussinessCount = placeBussinessCount;
    }

    public Integer getPlaceEconomComfortCount() {
        return placeEconomComfortCount;
    }

    public void setPlaceEconomComfortCount(Integer placeEconomComfortCount) {
        this.placeEconomComfortCount = placeEconomComfortCount;
    }

    public Integer getPlaceEconomCount() {
        return placeEconomCount;
    }

    public void setPlaceEconomCount(Integer placeEconomCount) {
        this.placeEconomCount = placeEconomCount;
    }
}
