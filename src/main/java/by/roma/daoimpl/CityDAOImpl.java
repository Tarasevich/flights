package by.roma.daoimpl;

import by.roma.HibernateSessionFactoryUtil;
import by.roma.dao.BaseDAO;
import by.roma.dao.CityDAO;
import by.roma.entity.Aircraft;
import by.roma.entity.City;
import by.roma.entity.Country;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;


import java.util.HashMap;
import java.util.List;

public class CityDAOImpl  implements CityDAO {

    private Session session;
    private Transaction transaction;

    @Override
    public City getCityById(Long id) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from City where id = :id");
        query.setParameter("id",id);
        City city = (City) query.getSingleResult();
        transaction.commit();
        session.close();
        return city;
    }

    @Override
    public City getCityByNameRu(String nameRu) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from City where nameRu = :name");
        query.setParameter("name",nameRu);
        City city = (City) query.getSingleResult();
        transaction.commit();
        session.close();
        return city;
    }

    @Override
    public City getCityByNameEn(String nameEn) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from City where nameEn = :name");
        query.setParameter("name",nameEn);
        City city = (City) query.getSingleResult();
        transaction.commit();
        session.close();
        return city;
    }

    @Override
    public List<String> getAllCitiesOfCountryRu(Country country) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("select City .nameRu from City as C join Country as Countr on C.country.id = Countr.id where Countr.id = :id");
        query.setParameter("id",country.getId());
        List<String> cities = query.list();
        transaction.commit();
        session.close();
        return cities;
    }

    @Override
    public List<String> getAllCitiesOfCountryEn(Country country) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("select City .nameEn from City as C join Country as Countr on C.country.id = Countr.id where Countr.id = :id");
        query.setParameter("id",country.getId());
        List<String> cities = query.list();
        transaction.commit();
        session.close();
        return cities;
    }

    @Override
    public List<String> getAllCitiesRu() {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("select nameRu from City");
        List<String> cities = query.list();
        transaction.commit();
        session.close();
        return cities;
    }

    @Override
    public List<String> getAllCitiesEn() {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("select nameEn from City");
        List<String> cities = query.list();
        transaction.commit();
        session.close();
        return cities;
    }

    public void add(City city) {
        BaseDAO.add(city);
    }

    public void delete(City city) {
        BaseDAO.delete(city);
    }

    public void update(City city) {
        BaseDAO.delete(city);
    }

}
