package by.roma.daoimpl;

import by.roma.HibernateSessionFactoryUtil;
import by.roma.dao.FlightDAO;
import by.roma.entity.Flight;
import by.roma.entity.Reservation;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.Date;
import java.util.List;

public class FlightDAOImpl implements FlightDAO {

    private Session session;
    private Transaction transaction;

    @Override
    public Flight getFlightById(Long id) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from Flight where id = :id");
        query.setParameter("id",id);
        Flight flight = (Flight) query.getSingleResult();
        transaction.commit();
        session.close();
        return flight;
    }

    @Override
    public Flight getFlightByNumber(String number) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from Flight where number = :number");
        query.setParameter("number",number);
        Flight flight = (Flight) query.getSingleResult();
        transaction.commit();
        session.close();
        return flight;
    }

    @Override
    public List<Flight> getFlightsByDepartDate(Date date) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from Flight where departDate = :date");
        query.setParameter("date",date);
        List<Flight> flight = query.list();
        transaction.commit();
        session.close();
        return flight;
    }

    @Override
    public List<Flight> getFlightsByArriveDate(Date date) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from Flight where arriveDate = :date");
        query.setParameter("date",date);
        List<Flight> flight = query.list();
        transaction.commit();
        session.close();
        return flight;
    }
}
