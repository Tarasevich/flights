package by.roma.daoimpl;

import by.roma.HibernateSessionFactoryUtil;
import by.roma.dao.BaseDAO;
import by.roma.dao.CountryDAO;
import by.roma.entity.Aircraft;
import by.roma.entity.City;
import by.roma.entity.Country;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class CountryDAOImpl implements CountryDAO {

    private Session session;
    private Transaction transaction;

    @Override
    public Country getCountryById(Long id) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from Country  where id = :id");
        query.setParameter("id",id);
        Country country = (Country) query.getSingleResult();
        transaction.commit();
        session.close();
        return country;
    }

    @Override
    public Country getCountryByNameRU(String name) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from Country  where nameRu = :name");
        query.setParameter("name",name);
        Country country = (Country) query.getSingleResult();
        transaction.commit();
        session.close();
        return country;
    }

    @Override
    public Country getCountryByNameEN(String name) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from Country  where nameEn = :name");
        query.setParameter("name",name);
        Country country = (Country) query.getSingleResult();
        transaction.commit();
        session.close();
        return country;
    }

    @Override
    public List<Country> getAllCountries() {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from Country");
        List<Country> countries = query.list();
        transaction.commit();
        session.close();
        return countries;
    }


    public void add(Country country) {
        BaseDAO.add(country);
    }

    public void delete(Country country) {
        BaseDAO.delete(country);
    }

    public void update(Country country) {
        BaseDAO.delete(country);
    }
}
