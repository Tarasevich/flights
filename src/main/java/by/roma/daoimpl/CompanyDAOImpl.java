package by.roma.daoimpl;

import by.roma.HibernateSessionFactoryUtil;
import by.roma.dao.BaseDAO;
import by.roma.dao.CompanyDAO;
import by.roma.entity.Company;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import java.util.List;


public class CompanyDAOImpl implements CompanyDAO {

    private Session session;
    private Transaction transaction;

    @Override
    public Company getCompanyById(Long id) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from Company where id = :id");
        query.setParameter("id",id);
        Company company = (Company) query.getSingleResult();
        transaction.commit();
        session.close();
        return company;
    }

    @Override
    public Company getCompanyByName(String name) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from Company where name = :name");
        query.setParameter("name",name);
        Company company = (Company) query.getSingleResult();
        transaction.commit();
        session.close();
        return company;
    }

    @Override
    public List<Company> getAllCompanies() {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from Company ");
        List<Company> companies = query.getResultList();
        transaction.commit();
        session.close();
        return companies;
    }

    public void add(Company company) {
        BaseDAO.add(company);
    }

    public void delete(Company company) {
        BaseDAO.delete(company);
    }

    public void update(Company company) {
        BaseDAO.update(company);
    }
}
