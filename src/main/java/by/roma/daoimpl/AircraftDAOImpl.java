package by.roma.daoimpl;

import by.roma.HibernateSessionFactoryUtil;
import by.roma.dao.AircraftDAO;
import by.roma.dao.BaseDAO;
import by.roma.entity.Aircraft;
import by.roma.entity.Company;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import java.util.List;

public class AircraftDAOImpl implements AircraftDAO {

    private Session session;
    private Transaction transaction;


    @Override
    public Aircraft getAirCraftById(Long id) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Aircraft aircraft = session.get(Aircraft.class,id);
        transaction.commit();
        session.close();
        return aircraft;
    }

    @Override
    public List<Aircraft> getListOfAircraftsByName(String name) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from Aircraft where name = :name");
        query.setParameter("name",name);
        List<Aircraft> aircrafts = query.getResultList();
        transaction.commit();
        session.close();
        return aircrafts;
    }

    @Override
    public List<Aircraft> getListOfAircraftsByCompany(Company company) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery
                ("from Aircraft as A  where A .company.id= :id");
        query.setParameter("id",company.getId());
        List<Aircraft> aircrafts =  query.list();
        transaction.commit();
        session.close();
        return aircrafts;
    }

    @Override
    public List<Aircraft> getAllAircrafts() {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from Aircraft ");
        List<Aircraft> aircrafts = query.getResultList();
        transaction.commit();
        session.close();
        return aircrafts;
    }

    @Override
    public Aircraft getAircraftByBoardNumber(String number) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from Aircraft as A where A.number = :number");
        query.setParameter("number",number);
        Aircraft aircraft = (Aircraft) query.getSingleResult();
        transaction.commit();
        session.close();
        return aircraft;
    }

    public void add(Aircraft aircraft) {
        BaseDAO.add(aircraft);
    }

    public void delete(Aircraft aircraft) {
        BaseDAO.delete(aircraft);
    }

    public void update(Aircraft aircraft) {
       BaseDAO.delete(aircraft);
    }
}
