package by.roma.daoimpl;

import by.roma.HibernateSessionFactoryUtil;
import by.roma.dao.DestinationDAO;
import by.roma.entity.Destination;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class DestinationDAOImpl implements DestinationDAO {

    private Session session;
    private Transaction transaction;

    @Override
    public Destination getDestinationById(Long id) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from Destination where id = :id");
        query.setParameter("id",id);
        Destination destination = (Destination) query.getSingleResult();
        transaction.commit();
        session.close();
        return destination;
    }

    @Override
    public Destination getDestinationByCityFrom(String cityFrom) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from Destination where cityFrom = :city");
        query.setParameter("city",cityFrom);
        Destination destination = (Destination) query.getSingleResult();
        transaction.commit();
        session.close();
        return destination;
    }

    @Override
    public Destination getDestinationByCityTo(String cityTo) {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from Destination where cityTo = :city");
        query.setParameter("city",cityTo);
        Destination destination = (Destination) query.getSingleResult();
        transaction.commit();
        session.close();
        return destination;
    }

    @Override
    public List<Destination> getAllDestinations() {
        session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from Destination");
        List<Destination> destination = query.list();
        transaction.commit();
        session.close();
        return destination;
    }
}
