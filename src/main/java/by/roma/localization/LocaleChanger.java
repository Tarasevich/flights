package by.roma.localization;


import javax.annotation.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Locale;

@ManagedBean
@SessionScoped
public class LocaleChanger implements Serializable {

    private Locale currentLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();

    public LocaleChanger(){

    }
    public void changeLocale(String locale){
        currentLocale = new Locale(locale);
    }
    public Locale getCurrentLocale(){
        return currentLocale;
    }




}
