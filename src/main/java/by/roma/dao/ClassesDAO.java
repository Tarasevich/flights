package by.roma.dao;

import by.roma.entity.Classes;

import java.util.List;

public interface ClassesDAO extends BaseDAO {

    Classes getClassById(Long id);
    Classes getClassByName(String name);
    String getClassDescriptionEN(String nameClass);
    String getClassDescriptionRU(String nameClass);
    List<Classes> getAllClasses();

}
