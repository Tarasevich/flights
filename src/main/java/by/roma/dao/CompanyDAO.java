package by.roma.dao;

import by.roma.entity.Aircraft;
import by.roma.entity.Company;

import java.util.List;

public interface CompanyDAO extends BaseDAO {

    Company getCompanyById(Long id);
    Company getCompanyByName(String name);
    List<Company> getAllCompanies();
}
