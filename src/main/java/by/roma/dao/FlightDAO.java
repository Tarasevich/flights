package by.roma.dao;

import by.roma.entity.Flight;
import by.roma.entity.Reservation;

import java.util.Date;
import java.util.List;

public interface FlightDAO extends BaseDAO {

    Flight getFlightById(Long id);
    Flight getFlightByNumber(String number);
    List<Flight> getFlightsByDepartDate(Date date);
    List<Flight> getFlightsByArriveDate(Date date);


}
