package by.roma.dao;

import by.roma.entity.Reservation;

import javax.sound.sampled.ReverbType;

public interface ReservationDAO extends BaseDAO {

    Reservation getReservationById(Long id);


}
