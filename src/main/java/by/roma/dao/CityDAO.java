package by.roma.dao;

import by.roma.entity.City;
import by.roma.entity.Country;

import java.util.List;

public interface CityDAO extends BaseDAO {

      City getCityById(Long id);
      City getCityByNameRu(String nameRu);
      City getCityByNameEn(String nameEn);
      List<String> getAllCitiesOfCountryRu(Country country);
      List<String> getAllCitiesOfCountryEn(Country country);
      List<String> getAllCitiesRu();
      List<String> getAllCitiesEn();
}
