package by.roma.dao;

import by.roma.entity.Aircraft;
import by.roma.entity.Company;

import java.util.ArrayList;
import java.util.List;

public interface AircraftDAO extends BaseDAO {

       Aircraft getAirCraftById(Long id);
       List<Aircraft> getListOfAircraftsByName(String name);
       List<Aircraft> getListOfAircraftsByCompany(Company company);
       List<Aircraft> getAllAircrafts();

       Aircraft getAircraftByBoardNumber(String number);




}
