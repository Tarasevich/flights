package by.roma.dao;

import by.roma.entity.City;
import by.roma.entity.Country;

import java.util.List;

public interface CountryDAO extends BaseDAO {

    Country getCountryById(Long id);
    Country getCountryByNameRU(String name);
    Country getCountryByNameEN(String name);
    List<Country> getAllCountries();


}
