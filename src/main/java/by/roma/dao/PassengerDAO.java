package by.roma.dao;

import by.roma.entity.Passenger;
import by.roma.entity.card.BankCard;

import java.util.List;

public interface PassengerDAO extends BaseDAO {

    Passenger getPassengerById(Long id);
    Passenger getPassengerByName(String firstName, String secondName);
    Passenger getPassengerByPassport(String passportNumber);
    List<BankCard> getAllPassengerCard(Passenger passenger);

}
