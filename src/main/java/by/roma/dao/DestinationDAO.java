package by.roma.dao;

import by.roma.entity.Destination;

import java.util.List;

public interface DestinationDAO extends BaseDAO {

    Destination getDestinationById(Long id);
    Destination getDestinationByCityFrom(String cityFrom);
    Destination getDestinationByCityTo(String cityTo);
    List<Destination> getAllDestinations();
}
